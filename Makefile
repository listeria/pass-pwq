PREFIX ?= /usr/local
SYSTEM_EXTENSION_DIR ?= /usr/lib/password-store/extensions
MANDIR ?= $(PREFIX)/share/man

PASSWORD_STORE_DIR ?= $(HOME)/.password-store
PASSWORD_STORE_EXTENSIONS_DIR ?= $(PASSWORD_STORE_DIR)/.extensions

ifeq ($(PASSWORD_STORE_ENABLE_EXTENSIONS), true)
  EXTENSION_DIR ?= $(PASSWORD_STORE_EXTENSIONS_DIR)
else
  EXTENSION_DIR ?= $(SYSTEM_EXTENSION_DIR)
endif

all:
	@echo "pass-pwq is a shell script, so there is nothing to do. Try \"make install\" instead."

install:
	@install -v -d "$(DESTDIR)$(MANDIR)/man1"
	@install -m 0644 -v man/pass-pwq.1 "$(DESTDIR)$(MANDIR)/man1/pass-pwq.1"
	@install -v -d "$(DESTDIR)$(EXTENSION_DIR)"
	@install -m 0755 -v src/pwq.bash "$(DESTDIR)$(EXTENSION_DIR)/pwq.bash"

uninstall:
	@rm -vf \
		"$(DESTDIR)$(EXTENSION_DIR)/pwq.bash" \
		"$(DESTDIR)$(MANDIR)/man1/pass-pwq.1"

.PHONY: all install uninstall
